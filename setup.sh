#!/usr/bin/env bash
dir=`dirname $0`
source $dir/bin/script_tools.sh
source $dir/config

section "Welcome"

for script in `find $dir/scripts/*.sh`; do
[ -f $script ] && source $script
done

section "Finished!"
