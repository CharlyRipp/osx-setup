OSX Setup
---------

### Installation

```
sh -c "$(curl -fsSL https://gitlab.com/CharlyRipp/osx-setup/raw/master/install.sh)"
```

#### Agents and Daemons
Some require `csrutil` to be disabled.
To do so, reboot holding Cmd+R to boot into recovery mode. 
Open terminal and run `csrutil disable`. 
Reenable with `csrutil enable` - recommended after you're all set up.


### TODOs

- Application Configs
    - [ ] Alfred
    - [ ] iTerm
    - [ ] IDE Configs/Plugins (Jetbrains / VS Code)
    - [ ] Mac fans control (settings and launch on startup)
- User Accounts (Possible?)
    - [ ] Mail
    - [ ] GPG and key
    - [ ] Keybase and folder plugin
    - [ ] Chrome
    - [ ] Jetbrains IDEs
- SSH Key Registration (Possible?)
    - [ ] GitLab
    - [ ] GitHub
- Repeatability Issues
    - [ ] Echo of .dotfiles/index.sh into .zshrc (terminal_environment)
- General Issues
    - [ ] Various Brew Installs ask for sudo password (Java, GPG)
    - [ ] Unattended install of ZSH launches ZSH, requiring you to `exit` to continue rest of setup
    - [ ] Clone of dotfiles prompts to add to known_hosts