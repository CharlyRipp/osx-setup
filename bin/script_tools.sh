#!/bin/bash

section() {
    txt="${1}"
    ch="${2:-#}"
    w=${3:-80}

    sep=$(printf "%0.s$ch" $(seq 1 $w))
    sec=$(( (${w} - ${#txt}) / 4 ))
    fill=$(( ${w} - (${sec} * 4 + ${#txt}) ))

    lh=${sep:1:$sec}
    ls=$(printf "%0.s " $(seq 1 $sec))
    rh=$lh
    rs=$ls

    [ $fill -ge 1 ] && rs="${rs} "
    [ $fill -ge 2 ] && ls="${ls} "
    [ $fill -eq 3 ] && rh="${rh}${ch}"

    echo -e "\n${sep}\n${lh}${ls}${txt}${rs}${rh}\n${sep}\n"
}

subsection() {
    section "${1}" "-"
}