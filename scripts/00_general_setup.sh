#!/usr/bin/env bash

section "General"
if [ $general_setup != "true" ]; then
    echo "Skipping..."
    return
fi

# Remove 'Last Login' message
touch ~/.hushlogin

if [ $passwordless_sudo = "true" ]; then
  echo "`whoami` ALL=(ALL) NOPASSWD: NOPASSWD: ALL" | sudo tee /etc/sudoers.d/`whoami` > /dev/null
fi

# Ensure xcode is installed early as it requires user interaction
if ! xcode-select --install 2>&1 | grep installed; then
  echo "Please install XCode, then check for system updates before we move on."
  read -p "Press enter when ready to continue"
  echo ""
fi
