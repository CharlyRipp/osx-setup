#!/usr/bin/env bash
section "Homebrew and Applications"
if [ $brew_install != "true" ]; then
    echo "Skipping..."
    return
fi

if ! [ -x "$(command -v brew)" ]; then
  subsection "Installing Homebrew"
  echo | ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

subsection "Updating"
brew update
brew install cask

subsection "Installing tap packages"
for tap in ${tap_packages[@]}; do
  brew tap ${tap}
done

subsection "Installing Java8"
brew cask install java8

subsection "Installing brew package"
brew install "${brew_packages[@]}"

subsection "Installing cask packages"
brew cask install "${fonts[@]}"
brew cask install --appdir="/Applications" "${cask_packages[@]}"

subsection "Cleaning up"
chmod go-w '/usr/local/share'
brew prune
brew cleanup
brew cask cleanup
brew doctor
