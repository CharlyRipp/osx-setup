#!/usr/bin/env bash
section "Setting up terminal environment"
if [ $terminal_environment != "true" ]; then
    echo "Skipping..."
    return
fi

subsection "Installing Oh-My-ZSH"
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
rm -f ~/.zcompdump;
/bin/zsh -i -c  "compinit -d ~/.zcompdump_custom"

if [ $setup_dotfiles = "true" ]; then
    git clone $DOTFILES_REPO_HTTPS ~/.dotfiles
    $(cd ~/.dotfiles && git remote remove origin && git remote add origin $DOTFILES_REPO_SSH)
    echo "source ~/.dotfiles/index.sh" >> ~/.zshrc
fi

// Generate key for this new box
ssh-keygen -t rsa -C "$email" -b 4096 -N "" -f ~/.ssh/id_rsa

// Setup terminal tools
git config --global user.email "$email"
git config --global user.name "$name"