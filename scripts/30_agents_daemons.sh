#!/usr/bin/env bash

### Adapted and personalized from the great https://gist.github.com/pwnsdx/d87b034c4c0210b988040ad2f85a68d3

# IMPORTANT: You will need to disable SIP aka Rootless in order to fully execute this script. Recommend you reenable it after.
## To do so, reboot holding Cmd+R to boot into recovery mode. Open terminal and run `csrutil disable`. Reenable with `csrutil enable`.
# WARNING: It might disable things that you may not like. Please double check the services in the TODISABLE vars.

# List active services: launchctl list | grep -v "\-\t0"
# Find a service: grep -lR [service] /System/Library/Launch* /Library/Launch* ~/Library/LaunchAgents

section "Configuring Agents and Daemons"
if [ $agents_daemons != "true" ]; then
    echo "Skipping..."
    return
fi

disabler() {
    services=("${!2}")
    for svc_name in ${services[@]}
    do
        svc=${1}/${svc_name}.plist
        if [[ -f ${svc} ]]; then
            if [[ -f ${svc}.disabled ]]; then
                echo "[ERR] ${svc_name} looks to be enabled and disabled - it may be self healing"
            else
                {
                    sudo launchctl unload -w ${svc}
                    launchctl unload -w ${svc}
                    sudo mv ${svc} ${svc}.disabled
                } &> /dev/null
                echo "[OK] ${svc_name} disabled"
            fi
        else
            if [[ ! -f ${svc}.disabled ]]; then
                echo "[WARN] ${svc} doesn't exist"
            fi
        fi
    done
}

enabler() {
    services=("${!2}")
    for svc_name in ${services[@]}
    do
        svc=${1}/${svc_name}.plist
        if [[ -f ${svc}.disabled ]]; then
            if [[ -f ${svc} ]]; then
                echo "[ERR] ${svc_name} looks to be enabled and disabled - it may be self healing. Attempting to enable."
            else
                sudo mv ${svc}.disabled ${svc%%.disabled*} &> /dev/null
            fi
            {
                sudo launchctl load -w ${svc}
                launchctl load -w ${svc}
            } &> /dev/null
            echo "[OK] ${svc_name} enabled"
        else
            if [[ ! -f ${svc} ]]; then
                echo "[WARN] ${svc} doesn't exist"
            fi
        fi
    done
}

disable-services() {
    disabler "/System/Library/LaunchAgents" AGENTS[@]
    disabler "/System/Library/LaunchDaemons" DAEOMONS[@]
}
enable-services() {
    enabler "/System/Library/LaunchAgents" AGENTS[@]
    enabler "/System/Library/LaunchDaemons" DAEOMONS[@]
}

# Default to disabling
disable-services