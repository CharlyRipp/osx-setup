echo "Ensure we have sudo access now, rather than later"
sudo -v

osx_tmp=`mktemp -d`
curl -s https://gitlab.com/CharlyRipp/osx-setup/-/archive/master/osx-setup-master.tar.gz | tar --strip-components=1 -xvf - -C $osx_tmp
read -p "Please read and edit the configuration carefully. Press enter to continue"
vi $osx_tmp/config
$osx_tmp/setup.sh

echo ""
read -p "Preserve these install files and config? [blank to delete or absolute path to move it to]: "
if [ -n $REPLY ]; then
    mkdir -p $REPLY/osx-setup && mv -f $osx_tmp/* $REPLY/osx-setup
fi
rm -fr $osx_tmp

echo "Some changes will not take effect until you reboot your machine."
read -p "Would you like to reboot now? [y/N]" -n 1
[[ $REPLY =~ ^[Yy]$ ]] && sudo reboot
echo ""